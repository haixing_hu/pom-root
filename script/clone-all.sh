#!/bin/bash

################################################################################
#
#  The script to clone all projects.
#
#  Author: Haixing Hu (starfish.hu@gmail.com)
#
################################################################################

GIT_SERVER_URL="git@bitbucket.org:haixing_hu/";

CURRENT_DIR=$(pwd);

# resolve links - $0 may be a softlink
THIS="$0";
while [ -h "$THIS" ]; do
  ls=$(ls -ld "$THIS");
  link=$(expr "$ls" : '.*-> \(.*\)$');
  if expr "$link" : '.*/.*' > /dev/null; then
    THIS="$link"
  else
    THIS=$(dirname "$THIS")/"$link";
  fi
done
THIS_DIR=$(dirname "$THIS");
cd "${THIS_DIR}";
THIS_DIR=$(pwd);
cd "${THIS_DIR}/../..";
PROJECT_HOME=$(pwd);
LOG_DIR="${PROJECT_HOME}/log";
if [ ! -d "${LOG_DIR}" ]; then
	mkdir -p "${LOG_DIR}";
fi

echo "";
echo "-----------------------------------------------------";
echo "";
PROJECT_LIST_FILE="${THIS_DIR}/project-list.txt";
for project in $(cat ${PROJECT_LIST_FILE}); do
    project_dir="${PROJECT_HOME}/${project}";
    if [ ! -d "${project_dir}" ]; then
        LOG_FILE="${LOG_DIR}/git-clone.${project}.log";
        echo "Try to clone ${project_dir} ...";
        if git clone ${GIT_SERVER_URL}${project}.git &> "${LOG_FILE}"; then
            echo "Successfully clone the project ${project}.";
        else
            echo "Failed to clone the project ${project}. ";
            echo "You may not have the permission.";
            echo "Please check the log file ${LOG_FILE} for details."
        fi
        echo "";
        echo "-----------------------------------------------------";
        echo "";
    fi
done
cd "${CURRENT_DIR}";
